const express = require('express')
const mongoose = require('mongoose')
const dotenv = require('dotenv')

dotenv.config()

const app = express()
const port = 4000;



// MongoDB Connection
mongoose.connect(`mongodb+srv://Archeon1221:${process.env.MONGODB_PASSWORD}@cluster0.l6bvvoi.mongodb.net/?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})





let db = mongoose.connection
db.on('error',() => console.error("Connection error!"))
db.on('open', () => console.log("Connected to MongoDB!"))
// MongoDB Connection End

app.use(express.json())
app.use(express.urlencoded({extended: true}))

// MongoDB Schemas
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: 'Pending'
	}
})

// MongoDB Schemas END

// MongoDB Model
const Task = mongoose.model('Task', taskSchema)

// MongoDB Model END

// Routes

app.post('/tasks', (request, response) => {
	Task.findOne({name: request.body.name}, (error, result) => {
		if(result !== null && result.name == request.body.name){ return response.send('Duplicate task found!')}

			let newTask = new Task({
				name: request.body.name
			})

			newTask.save((error, savedTask) => {
				if (error){
					return console.error(error)
				}
				else {
					return response.status(200).send('New task created!')
				}
			})	
	})

})


app.get('/tasks', (request, response) => {
	Task.find({}, (error, result) => {
		if(error){
			return console.log(error)
		}

		return response.status(200).json({
			data: result
		})
	})
})

// Routes END


// S35 ACTIVITY

// Create a User schema

const userSchema = new mongoose.Schema({
	username: String,
	password: String,
	status: {
		type: String,
		default: 'Pending'
	}
})

// Create a User model

const User = mongoose.model('User', userSchema)

// Create a POST route that will access the /signup route that will create a user
// Process a POST request at the /signup route using postman to register a user.



app.post('/signup', (request, response) => {
	User.findOne({username: request.body.username}, (error, result) => {
		if(result !== null && result.username == request.body.username){ return response.send('Duplicate user found!')}

			let newUser = new User({
				username: request.body.username,
				password: request.body.password
			})

			newUser.save((error, savedTask) => {
				if (error){
					return console.error(error)
				}
				else {
					return response.status(200).send('New user registered!')
				}
			})	
	})

})

**//

app.listen(port, () => console.log(`Server running at localhost: ${port}`))